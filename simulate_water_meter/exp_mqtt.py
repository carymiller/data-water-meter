import time
from exploratorium import make_session
from simulate_device import fake_water_flow
import paho.mqtt.client as mqtt
import logging

log = logging.getLogger()
log.setLevel(logging.INFO)


session_args = dict(
        profile_name = 'poweruser-47lining-test5',
        region_name = 'us-east-1',
        )

try: this
except NameError:
    this = make_session(**session_args)
    iot = this.session.client('iot')
mqttc = mqtt.Client()

endpoint = 'a3eb1jp9ei28id.iot.us-east-1.amazonaws.com'


if 0:
    d = iot.list_things()
    [dct] = d['things']
    description = iot.describe_thing(thingName=dct['thingName'])
    # That still does not give an endpoint for sending mqtt messages.
    # I need that.

def on_connect(client, userdata, flags, rc):
    '''when the client receives a CONNACK response from the server.
    '''
    print("on_connect %s" % rc)


def on_message(client, userdata, msg):
    '''when a PUBLISH message is received from the server.
    '''
    print('on_message', msg.topic, msg.payload)


def on_publish(client, userdata, mid):
    '''
    '''
    print("on_publish")


def on_subscribe(mqttc, obj, mid, granted_qos):
    '''
    '''
    print("on_subscribe")


mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Looks like we need a lot of stinking credentials here. ala
# https://stackoverflow.com/questions/36721132/how-to-troubleshoot-mqtt-and-aws-iot


sdir = '/home/cary/.ssh/iot_button_test5_us_east_1/'


from os.path import join, exists
import ssl

'''
root     = join(sdir, 'symantec_root_ca.pem')
certfile = join(sdir, '74c0205ddf-certificate.pem.crt')
keyfile  = join(sdir, '74c0205ddf-private.pem.key')
for fpath in [root, certfile, keyfile]:
    print(fpath)
    assert exists(fpath)
'''


mqttc.tls_set( join(sdir, 'symantec_root_ca.pem'),
    certfile = join(sdir, '74c0205ddf-certificate.pem.crt'),
    keyfile =  join(sdir, '74c0205ddf-private.pem.key'),
    tls_version = ssl.PROTOCOL_TLSv1_2,
    ciphers = None
    )


# blocking
#    mqttc.connect(endpoint)
# blocking
# non-blocking
mqttc.connect_async(endpoint)
# non-blocking
mqttc.loop_start()
import random
i = 0
while i<5:
    i+=1
    ob = mqttc.publish("paho/temperature", random.choice(range(12)))
    assert ob.is_published() == False


# from aws docs
# A WebSocket connection is initiated on a client by sending an HTTP GET
# request. The URL you use is of the following form:
region='us-east-1'
#        wss://<endpoint>.iot.<region>.amazonaws.com/mqtt
url = 'wss://%(endpoint)s.iot.%(region)s.amazonaws.com/mqtt' % locals()

'''
 ~/.ssh/iot_button_test5_us_east_1/
 total 56
 -r-------- 1 cary cary 1220 Dec  6 13:42 74c0205ddf-certificate.pem.crt
 -r-------- 1 cary cary 1675 Dec  6 13:42 74c0205ddf-private.pem.key
 -r-------- 1 cary cary 1732 Dec  6 13:45 symantec_root_ca.pem

mqttc.tls_set("/home/pi/deviceSDK/root-CA.crt",
    certfile="/home/pi/deviceSDK/7391d7d21d-certificate.pem.crt",
    keyfile="/home/pi/deviceSDK/7391d7d21d-private.pem.key",
    tls_version=ssl.PROTOCOL_TLSv1_2,
    ciphers=None
)
'''



