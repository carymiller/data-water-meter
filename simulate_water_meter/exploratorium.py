from __future__ import print_function
import boto3
from botocore.exceptions import ClientError, ProfileNotFound
from functools import wraps
#print('boto3 version %s' % boto3.__version__)

tab = ' '*4
globalize = False
sessions = {}


def stashed_session(profile_name=None, **kwargs):
    '''
    '''
    if profile_name:
        kwargs['profile_name'] = profile_name
    key = frozenset(tuple(sorted(kwargs.items())))
    if key not in sessions:
        print('key %s' % key)
        try:
            sessions[key] = boto3.session.Session(**kwargs)
        except ProfileNotFound:  # running on an ec2 instance.  or?
            kwargs.pop('profile_name')
            sessions[key] = boto3.session.Session(**kwargs)
    return sessions[key]



def make_session(profile_name=None, region_name=None):
    '''a bit of a move in the OO direction.   Functions that use it look a lot
    like methods.
    >>> this = make_session(profile_name='hhc-datalake-PowerUser', region_name='us-west-2')
    >>> this = make_session(profile_name='poweruser-47lining-test5', region_name='ca-central-1')
    >>> this = make_session(profile_name='Administrator-prologis-development', region_name='us-east-1')

    >>> client = this.session.client('ssm')
    '''
    this = object()   # NO
    # I want the simplest generic object I can assign attributes to.   All below
    # fill the bill.
    this = lambda:None
    this = type('', (), {})    # an 'anonymous' class
    this = type('', (), {})()  # instance of anonymous class
    params = locals()
    params.pop('this')
    this.session = stashed_session(**params)
    this.profile_name = profile_name 
    this.params = params
    return this


# ############################################################################ #
#                Below is about inspecting ec2 instances                       #
#                   No it aint
#               I'm now interested in inspecting and aggregating s3 objects.
#               NO
#               Now even more interested in kinesis streams/firehose.
#               With kinesis we can aggregate BEFORE posting to s3.
#               HaHa!
# ############################################################################ #
# https://stackoverflow.com/questions/19290103/merging-files-on-aws-s3-using-apache-camel
# Lots to think about when aggregating s3 files.   Bandwidth issues.
# sidestepping that.


def remember(func):
    cache = {}
    @wraps(func)
    def inner(*args,  **kwargs):
        key = frozenset((args, tuple(sorted(list(kwargs)))))
        if key not in cache:
             cache[key] = func(*args,  **kwargs)
        return cache[key]
    return inner


def header(name):
    '''
    >>> header('foo bar')

    ====================== foo bar ======================
    '''
    attn = '=' * 22
    print('\n%s %s %s' % (attn, name, attn))


def name_of(thing):
    try:
        return thing.name
    except AttributeError:
        return name_from_meta_data(thing)


def name_from_meta_data(thing):
    for dct in thing.meta.data['Tags']:
        if dct['Key'] == 'Name':
            return dct['Value']


def show_container_attribute(seq, attr):
    '''
    Many attributes of boto3 objects are container objects having an all() method.
    seq:  is a sequence of boto3 objects.   
        thing.attr is a container object having an all() method.
    for thing in seq:
        Show all elements of thing.attr

    attr in 'policies attached_policies instance_profiles' for roles

    '''
    # TODO:  show an example
    header(attr)
    for thing in seq:
        print('%s%s' % ('', name_of(thing)))
        for thing in getattr(thing, attr).all():
            print('%s%s' % (tab, thing))
        print('')
    if globalize:
        globals().update(locals())
    

def class_name_of(seq):
    '''I want the classname of the items in seq,  but I'll settle for the
    classname of seq if seq is not indexable.
    '''
    try:
        thing = seq[0]
    except TypeError:  # seq not indexable
        thing = seq
    return thing.__class__.__name__


def show_ordinary_attribute(seq, attr, subattr=None):
    '''Given a sequence of objects and an attribute name print the name of the object
    and the value of the attribute for each object in the sequence.
    '''
    # TODO:  show an example
    header('%s %s' % (class_name_of(seq), attr))
    result = []
    for thing in seq:
        print(name_of(thing))
        value = getattr(thing, attr)
        if subattr:
            value = subattr(value)
        print('%s%s' % (tab, value))
        print('')
        result.append((name_of(thing), value))
    return result



if __name__ == '__main__':

    # Get optional command-line args.
    import argparse
    parser = argparse.ArgumentParser()
    args = parser.parse_args()

    
    profiles = '''
    hhc-datalake-PowerUser
    hhc-datalake-Administrator
    credstash-reader-p-47lining-test7
    cmiller-47lining-identity
    poweruser-47lining-test7
    Administrator-prologis-development
    '''.split()

    if 0:
        from exploratorium import make_session
        region = 'us-west-2'
        region = 'us-east-1'
        profile = 'Administrator-prologis-development'
        this = make_session(profile_name=profile, region_name=region)
        client = this.session.client('kinesis')
        conn = client
        for stream_name in client.list_streams()['StreamNames']:
            print(stream_name)

        r = client.describe_stream(StreamName=stream_name)
        s = r['StreamDescription']['StreamStatus']


if 0:
    region = 'us-east-1'
    profile = 'Administrator-prologis-development'
    try: this
    except NameError:
        this = make_session(profile_name=profile, region_name=region)
        ssm = client = this.session.client('ssm')
        parameters = 'snowflake-lambda-user snowflake-lambda-account snowflake-lambda-password'.split()

        dct = ssm.get_parameters(Names=parameters, WithDecryption=True)
        ps = dct['Parameters']
        for dct in ps:
            dct.pop('Type')
            

    snowflake_url = ''


