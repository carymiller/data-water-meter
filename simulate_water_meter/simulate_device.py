'''
Simulate data from IoT devices.  In this case, two different types of
electrical current sensors and one water flow sensor.

'''

from __future__ import print_function
import csv
import json
import collections
import os
import random
import sys
import time
from fake_water import gpm, gallons_this_month, signal_strength
import fake_water

sys.maxint = sys.maxsize  # sort of a monkey patch


if '__file__' not in locals():
    import inspect
    __file__ = inspect.getframeinfo(inspect.currentframe()).filename


def make_thing(lst):
    '''
    payload looks like this...
    {
    "_|timestamp"      : "1459874949463",
    "s|color"          : "red",
    "i|counter"        : "1246",
    "f|temperature"    : "60.5",
    "b|valid"          : "true",
    "d|expiredDate"    : "09/01/2016",
    "i2|dimension"     : "1920|1080",
    "f3|3dCoords"      : "23.3|24.2|83.1",
    }
    '''
    dm = dict(Float='f')
    name, typ, val = lst
    return ('%s|%s' % (dm[typ], name), val)


def fake_water_flow(cfg):
    '''
    >>> fpath = '/opt/selene/config/devices/dynatfxu-0.properties'
    >>> fake_water_flow(fpath)
    '''
    dct = {}
    dct['_|timestamp'] = int(time.time())
    dct['f|flow_rate_gpm'] = gpm()
    dct['f|net_totalizer_g'] = gallons_this_month()
    dct['f|signal_strength_p'] = signal_strength()
    dct['s|DeviceName'] = cfg["name"]
#    dct["s|deviceId"] = cfg["name"].split("-")[1]
    dct["s|telemetryId"] = random.randint(0, sys.maxint)
    return json.dumps(dct)


def check_fake():
    cfg = dict(name='fubar')
    dct = json.loads(fake_water_flow(cfg))
    for key in sorted(dct):
        print(key, dct[key])


def repeat_fake():
    ts = 1513692891
    for i in range(5): 
        ts += i
        time.time = lambda: ts
        check_fake()
        print('')


if __name__ == '__main__':
    print(fake_water_flow(dict(name='barfoo')))
    print('')
    repeat_fake()
