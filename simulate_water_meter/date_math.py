'''
Variations on time-telling.
'''
import time
import datetime


def current_datetime():
    return datetime.datetime.fromtimestamp(time.time())


def dhm():
    '''return day-of-month, hour-of-day, minute-of-hour
    '''
    dt = current_datetime()
    return (dt.day, dt.hour, dt.minute)


def dm():
    '''return day-of-month, minute-of-day
    '''
    d, h, m = dhm()
    return (d, 60*h + m)


def mm():
    '''return minute-of-month
    '''
    d, m = dm()
    return 60*24*(d-1) + m
minute_of_month = mm


def weekday():
    return current_datetime().weekday() in range(5)


def day_name():
    return current_datetime().strftime('%a')


def demo():
    print('current_datetime', current_datetime())
    print('dhm', dhm())
    print('dm', dm())
    print('mm', mm())
    print('day_name', day_name())
    print('weekday', weekday())
    print('')



if __name__ == '__main__':
    demo()
    for i in range(12,19):
        current_datetime = lambda: datetime.datetime(2017, 12, i, 14, 59)
        demo()


