import time
from exploratorium import make_session
from simulate_device import fake_water_flow
import logging
log = logging.getLogger()
log.setLevel(logging.INFO)


session_args = dict(
        profile_name = 'poweruser-47lining-test5',
        region_name = 'us-east-1',
        )
sqs_info = dict(
        url = 'https://queue.amazonaws.com/466244873177/some-q-1',
        )
firehose_info = dict(
        DeliveryStreamName = 'fake-data-stream-cmtest-fh2',
        )

msg = 'test message from boto at %s'


try: this
except NameError:
    this = make_session(**session_args)
    sqs = this.session.client('sqs')
    firehose = this.session.client('firehose')
    iot = this.session.client('iot')

dcts = [
        dict(name='bar'),
        dict(name='foo'),
        dict(name='barfoo'),
        dict(name='foobar'),
        dict(name='foox'),
        dict(name='x'),
        dict(name='berfoo'),
    ] # n == 7


def send_several():
    for dct in dcts:
        msg = fake_water_flow(dct)
        sqs.send_message(QueueUrl=sqs_info['url'],  MessageBody=msg)
#        print('sent message %s' % msg)


def send_batch():
    Entries = [dict(MessageBody=fake_water_flow(dct), Id=str(i)) for (i, dct) in enumerate(dcts)]
    sqs.send_message_batch(QueueUrl=sqs_info['url'],  Entries=Entries)
#    print('sent batch %s' % Entries)


def send_bunches(n, fun=send_several):
    t0 = time.time()
    for i in range(n):
#        print(i)
        fun()
    t1 = time.time()
    print('total messages sent == %s' % (n*len(dcts)))
    print('%s et %s' % (fun.__name__, t1-t0))


def get_several(n):
    i = 0
    t0 = time.time()
    while i < n:
        i+=1
        response = sqs.receive_message(QueueUrl=sqs_info['url'])
#        for message in response['Messages']: print(i, message['Body'])
    t1 = time.time()
    print('total messages received == %s' % n)
    print('et %s' % (t1-t0,))
    # Pulling is even slower than pushing.  Max seen so far is 1000 msg/90 sec
    # => 11 msg/sec


# ################### firehose #################### #


def send_several_to_firehose():
    for dct in dcts:
        msg = fake_water_flow(dct)
#        sqs.send_message(QueueUrl=sqs_info['url'],  MessageBody=msg)
        response = firehose.put_record(
            DeliveryStreamName = firehose_info['DeliveryStreamName'],
            Record = {'Data': msg},
        )
        log.debug('sent message %s' % msg)
        log.debug('     got response %s\n' % response)


def send_batch_to_firehose():
    Records = [dict(Data=fake_water_flow(dct)) for (i, dct) in enumerate(dcts)]
    response = firehose.put_record_batch(
        DeliveryStreamName = firehose_info['DeliveryStreamName'],
        Records = Records
    )
    log.warning('sent batch %s' % Records)



def peek():
    QueueUrl = sqs_info['url']
    AttributeNames = ['All',]
    return sqs.get_queue_attributes(**locals())


def current():
    for i in range(4): pass  # > 5 min
    for i in range(3): pass
    for i in range(2): 
        print('n == %s' % i)
        send_bunches(10**i, send_several)
        send_bunches(10**i, send_batch)
        print('')
    atts = peek()
    nmsg = atts['Attributes']['ApproximateNumberOfMessages']
    print('nmsg', nmsg)


def push_one():
    response = firehose.put_record(
        DeliveryStreamName=firehose_info['DeliveryStreamName'],
        Record={'Data': b'yoohoo from boto3'}
    )


def moldy():
    t0 = time.time()
    send_several()
    print('several et = %s' %(time.time()-t0))
    t0 = time.time()
    send_batch()
    print('batch et = %s' %(time.time()-t0))

# Fastest time for 700 messages == 12.2 sec == 57 msg/sec
# Fastest time for 70 messages == 1.0 sec == 70 msg/sec

send_batch()
send_several()
send_bunches(333)
#send_several_to_firehose()
#send_batch_to_firehose()
print(peek()['Attributes']['ApproximateNumberOfMessages'])

    


