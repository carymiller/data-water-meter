import random
import time
import datetime
import date_math
from date_math import dhm, dm
from fakery import spiky


out_example = '''
_|timestamp 1509090852982
f|net_totalizer_g 144612.8561
f|signal_strength_p 91.96
f|flow_rate_gpm 6.175
s|telemetryId 59f2e625e0c2b6ae52c2bd2d
s|deviceId 59dfc3a1a894ed642d5414ff
s|DeviceName DynaTFXU-DSTFXU0000000
'''


'''
I'm thinking 
    time-of-day for gpm
    time-of-month for g

gpm
    Mostly 0 6pm-6am
    Mostly constant 6am-6pm at say 5 gpm

g
    Increasing from 0 at start of month.

'''




def gpm():
    '''Mostly off during non-workday hours.
    '''
    (d, h, m) = dhm()
    if h < 6 or h > 17:
        return spiky(u0, u1, pct)
    return random.normalvariate(mu,sd)


def gallons_this_month():
    '''Sawtooth, increasing all month.
    '''
    d, m = dm()
    minute_of_month = 60*24*(d-1) + m
    return minute_of_month * random.normalvariate(mu/1.8,sd*0.1)


def signal_strength():
    '''in [0,1] mostly near .9
    '''
    if not random.choice(range(N)):
        return 0
    if random.choice(range(N)):
        return random.normalvariate(.9,.01)
    return random.uniform(0,1)



mu = 5
sd = 1
u0 = 1
u1 = 4
pct = .1
N = int(1/pct)



def demo():
    '''
    '''
    print('gallons this month', gallons_this_month())
    print('               gpm', gpm())
    print('   signal_strength', signal_strength())


if __name__ == '__main__':
    demo()


