'''
from simu.py
'''
import random


def spiky(u0, u1, pct):
    '''Mostly return 0 with the occasional flush and very occasional much bigger
    flush.
    pct:  percent of obs to return 0
    if not returning 0, return uniform on interval [u0, u1]

    >>> [spiky(0,1,.5) for i in range(11)]
    [0, 0, 0.06980901939789752, 0.4895344834749772, 0.061165357369565965, 0,
        3.717080946718662, 0.9805076392043843, 0, 0, 0.4105042467218937]
    '''
    if random.uniform(0,1) > pct:
        return 0
    return random.uniform(u0,u1)


def demo_spiky():
    '''
    '''
    u0 = 1
    u1 = 14
    pct = .1
    done = False
    i = 0
    while not done:
        i += 1
        s = spiky(u0, u1, pct)
        print(i, s)
        done = True if s > 10 else False


if __name__ == '__main__':
    demo_spiky()

