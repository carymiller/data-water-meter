# save the old archive
mv ec2_simulate.tar ec2_simulate.tar.$(date +%s)  2>/dev/null

# make the new archive
tar -cf ec2_simulate.tar simulate_*

# Problem.  aws does not run properly inside a script.
# This alias should fix that but does not.
# alias aws='aws --profile administrator-47lining-test5  --region us-east-1'

aws='aws --profile poweruser-47lining-test5  --region us-east-1'

# copy archive to s3
$aws s3 cp ec2_simulate.tar s3://cm-fake-data-code/ec2_simulate.tar

# check timestamp on archive in s3.
#$aws s3 ls s3://cm-fake-data-code/ec_simulate.tar

