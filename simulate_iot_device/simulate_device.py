'''
Simulate data from IoT devices.  In this case, two different types of
electrical current sensors and one water flow sensor.
Read the simulated data from csv files, one for each device type.
'''

from __future__ import print_function
import csv
import json
import collections
import os
import random
import sys
import time
from fake_water import gpm, gallons_this_month, signal_strength

sys.maxint = sys.maxsize  # sort of a monkey patch


if '__file__' not in locals():
    import inspect
    __file__ = inspect.getframeinfo(inspect.currentframe()).filename


def read(fname):
    dpath = os.path.dirname(os.path.abspath(__file__))
#    with open(os.path.join(dpath,fname), 'rb') as csvfile:
    with open(os.path.join(dpath, 'csv',fname)) as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
#        try: head = reader.next()
#        except: head = next(reader)
        head = next(reader)
        return (head, [row for row in reader])


def make_thing(lst):
    '''
    payload looks like this...
    {
    "_|timestamp"      : "1459874949463",
    "s|color"          : "red",
    "i|counter"        : "1246",
    "f|temperature"    : "60.5",
    "b|valid"          : "true",
    "d|expiredDate"    : "09/01/2016",
    "i2|dimension"     : "1920|1080",
    "f3|3dCoords"      : "23.3|24.2|83.1",
    ....
    So most rows in the payload is row[3:-1] in the spreadsheet.
    }
    '''
    dm = dict(Float='f')
    name, typ, val = lst
    return ('%s|%s' % (dm[typ], name), val)


def go(fname):
    '''Return list of records in fname, csv file.
    '''
    (head, rows) = read(fname)
    ps = collections.namedtuple('ps', head)
    records = []
    record = {'s|DeviceName':None}  # to get things started.
    for row in rows:
        d = ps(*row)
        if d.DeviceName != record['s|DeviceName']:  # new record
            if record['s|DeviceName']:   # ignore the initial placeholder.
#                print(record['s|DeviceName'])
                records.append(json.dumps(record))
            record = {}
            record['s|DeviceName'] = d.DeviceName
            record['s|deviceId'] = d.deviceId
            record['s|telemetryId'] = d.telemetryId 
            record['_|timestamp'] = d.timestamp
        key, value = make_thing(row[3:-1])
        record[key] = value
    records.append(json.dumps(record))  # to wrap things up.
#    print(record['s|DeviceName'])
    return records


fname = 'powerscout3037.csv'
fname = 'dynasonicTFX-Ultra.csv'
fname = 'powerscout24.csv'
fnames = 'powerscout3037.csv dynasonicTFX-Ultra.csv powerscout24.csv'.split()
all_records = []
for fname in fnames:
    records = go(fname)
    all_records.extend(records)


# So that gets all the simulated data.  Now the functions.


def fake_water_flowxxxxxxxxxxxxxxxxxxxxxxx():
    from fake_water import gpm, gallons_this_month, signal_strength
    dct = {}
    dct['_|timestamp'] = int(time.time())
    dct['f|flow_rate_gpm'] = gpm()
    dct['f|net_totalizer_g'] = gallons_this_month()
    dct['f|signal_strength_p'] = signal_strength()
    dct['s|telemetryId'] = '59f2e625e0c2b6ae52c2bd2d'
    dct['s|deviceId'] = '59dfc3a1a894ed642d5414ff' 
    dct['s|DeviceName'] = 'DynaTFXU-DSTFXU0000000' 
    return json.dumps(dct)


def config_dict(config_file_path):
    with open(config_file_path) as fh:
        lines = fh.readlines()
    return dict(line.strip().split('=') for line in lines)


def fake_water_flow(config_file_path):
    '''
    >>> fpath = '/opt/selene/config/devices/dynatfxu-0.properties'
    >>> fake_water_flow(fpath)
    '''
    cfg = config_dict(config_file_path)
    dct = {}
    dct['_|timestamp'] = int(time.time())
    dct['f|flow_rate_gpm'] = gpm()
    dct['f|net_totalizer_g'] = gallons_this_month()
    dct['f|signal_strength_p'] = signal_strength()
#    dct['s|telemetryId'] = '59f2e625e0c2b6ae52c2bd2d'
#    dct['s|deviceId'] = '59dfc3a1a894ed642d5414ff' 
#    dct['s|DeviceName'] = 'DynaTFXU-DSTFXU0000000' 
    dct['s|DeviceName'] = cfg["name"]
    dct["s|deviceId"] = cfg["name"].split("-")[1]
    dct["s|telemetryId"] = random.randint(0, sys.maxint)

#    payload["s|name"] = config_dict["name"]
#    payload["s|deviceId"] = config_dict["name"].split("-")[1]
#    payload["s|telemetryId"] = str(random.randint(0, sys.maxint))
    return json.dumps(dct)


def fake_ps_24_a(config_file_path):
    cfg = config_dict(config_file_path)
    dct = {}
    dct['_|timestamp'] = int(time.time())
    dct['s|DeviceName'] = cfg["name"]
    dct["s|deviceId"] = cfg["name"].split("-")[-1]
    dct["s|telemetryId"] = random.randint(0, sys.maxint)

    a, b, c = [f() * 24 for f in [gallons_this_month]*3]
    dct['f|a_energy_l0_kvah'] = a
    dct['f|a_energy_l1_kvah'] = b
    dct['f|a_energy_l2_kvah'] = c
    dct['f|a_energy_sys_kvah'] = a+b+c+random.uniform(0,1)

    a, b, c = [f() / 24 for f in [gpm]*3]
    dct['f|a_power_l0_kva'] = a
    dct['f|a_power_l1_kva'] = b
    dct['f|a_power_l2_kva'] = c
    dct['f|a_power_sys_kva'] = a+b+c

    a, b, c = [f() / 5 for f in [gpm]*3]
    dct['f|current_l0_a'] = a
    dct['f|current_l1_a'] = b
    dct['f|current_l2_a'] = c
    dct['f|current_avg_sys_a'] = a+b+c + random.normalvariate(0, 0.1)

    a, b, c = [f() / 32 for f in [gallons_this_month]*3]
    dct['f|energy_l0_kwh'] = a
    dct['f|energy_l1_kwh'] = b
    dct['f|energy_l2_kwh'] = c
    dct['f|energy_sys_kwh'] = a+b+c + random.normalvariate(0, 3)

    dct['f|frequency_hz'] = 60 + random.normalvariate(0, 1)
    return json.dumps(dct)


def fake_ps_24_b(config_file_path):
    cfg = config_dict(config_file_path)
    dct = {}
    dct['_|timestamp'] = int(time.time())
    dct['s|DeviceName'] = cfg["name"]
    dct["s|deviceId"] = cfg["name"].split("-")[-1]
    dct["s|telemetryId"] = random.randint(0, sys.maxint)

    a, b, c = [f() / 25 for f in [gpm]*3]
    dct['f|power_l0_kw'] = a
    dct['f|power_l1_kw'] = b
    dct['f|power_l2_kw'] = c
    dct['f|power_sys_kw'] = a+b+c

    a, b, c = [f() / 32 for f in [gallons_this_month]*3]
    dct['f|r_energy_l0_kvarh'] = a
    dct['f|r_energy_l1_kvarh'] = b
    dct['f|r_energy_l2_kvarh'] = c
    dct['f|r_energy_sys_kvarh'] = a+b+c + random.normalvariate(0, 3)

    a, b, c = [f() / 25 for f in [gpm]*3]
    dct['f|r_power_l0_kvar'] = a
    dct['f|r_power_l1_kvar'] = b
    dct['f|r_power_l2_kvar'] = c
    dct['f|r_power_sys_kvar'] = a+b+c

    a, b, c = [f() / 25 for f in [gpm]*3]
    dct['f|voltage_l0ton_v'] = a
    dct['f|voltage_l1ton_v'] = b
    dct['f|voltage_l2ton_v'] = c
    dct['f|voltage_ltol_avg_v'] = a+b+c + random.normalvariate(0, 1) 
    dct['f|voltage_lton_avg_v'] = a+b+c + random.normalvariate(0, 3) 

    return json.dumps(dct)


#def check_fake(dct=dict(ts=1509551990-60*60*6)): pass
def check_fake():
    dct=dict(ts=1509551990-60*60*6)
    print(dct)
    print(dct)
    print(dct)
#    time.time = lambda: dct['ts']  # why was I monkey patching this?
    dct['ts'] += 1
    fpath = '/opt/selene/config/devices/dynatfxu-0.properties'
    dct = json.loads(fake_water_flow(fpath))
    for key in sorted(dct):
        print(key, dct[key])


def repeat_fake():
    for i in range(5): 
        check_fake()
        print('')


repeat_fake()
