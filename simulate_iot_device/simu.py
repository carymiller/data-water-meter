import random
import time
import datetime


def dhm():
    '''return day-of-month, hour-of-day, minute-of-hour
    '''
    dt = datetime.datetime.fromtimestamp(time.time())
    return (dt.day, dt.hour, dt.minute)


def dm():
    '''return day-of-month, minute-of-day
    '''
    d, h, m = dhm()
    return (d, 60*h + m)


def mm():
    '''return minute-of-month
    '''
    d, m = dm()
    return 60*24*(d-1) + m
minute_of_month = mm


def spiky(u0, u1, pct):
    '''Mostly return 0 with the occasional flush and very occasional much bigger
    flush.
    >>> [spiky(0,1,.5) for i in range(11)]
    [0, 0, 0.06980901939789752, 0.4895344834749772, 0.061165357369565965, 0,
        3.717080946718662, 0.9805076392043843, 0, 0, 0.4105042467218937]
    '''
    if random.uniform(0,1) > pct:
        return 0
    x = random.uniform(u0,u1)
    if random.uniform(0,1) < pct:
        return 10*x
    return x


