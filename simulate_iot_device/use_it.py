from simulate_device import fake_water_flow, fake_ps_24_a, fake_ps_24_b, config_dict
import json
import os
import socket
import time
from glob import glob
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
config_prefix = '/opt/selene/config/devices/'


def show(func):
    stuff = json.loads(func())
    for key in sorted(stuff):
        print(key, stuff[key])


stuffs = (
("powerscout-3037*", None),
("dynatfxu-*", fake_water_flow),
("powerscout-24*", fake_ps_24_a),
)
stuffs = (
("dynatfxu-*", fake_water_flow),
("powerscout-24*", fake_ps_24_b),
)
stuffs = (
("dynatfxu-*", fake_water_flow),
("powerscout-24*", fake_ps_24_a),
("powerscout-24*", fake_ps_24_b),
)
stuffs = (
("dynatfxu-*", fake_water_flow),
)



def go():
    for (file_prefix, func) in stuffs:
        config_files = glob(os.path.join(config_prefix, file_prefix))
        print('')
        print('')
        for fpath in config_files:

            print(time.ctime())
            print(fpath)
            print(func.__name__)
            continue
            break
            payload = func(fpath)
            show(lambda:payload)
            port = int(config_dict(fpath)["port"])
            print('port', port)
            print('')
            sock.sendto(payload, ("127.0.0.1", port))
            time.sleep(1)


#go()


def portscan():
    s = socket.socket()
    host = "127.0.0.1"
    for (file_prefix, func) in stuffs:
        config_files = glob(os.path.join(config_prefix, file_prefix))
        for fpath in config_files:
            cd = config_dict(fpath)
            port = int(cd["port"])
#            print(cd.keys())
            print('   ', cd["name"])
            print('    host', host)
            print('    port', port)
            print(s.connect((host,port)))

    # sudo nmap -sU -p port target

#portscan()


def tok():
    '''
    '''
    # TODO:  send to kinesis.
    for (file_prefix, func) in stuffs:
        config_files = glob(os.path.join(config_prefix, file_prefix))
        print('')
        print('')
        for fpath in config_files:
            if 'powerscout' in fpath: 
                continue
            payload = func(fpath)
            print(payload)
            print('')


tok()



