import random
import time
import datetime


out_example = '''
_|timestamp 1509090852982
f|net_totalizer_g 144612.8561
f|signal_strength_p 91.96
f|flow_rate_gpm 6.175
s|telemetryId 59f2e625e0c2b6ae52c2bd2d
s|deviceId 59dfc3a1a894ed642d5414ff
s|DeviceName DynaTFXU-DSTFXU0000000
'''


'''
I'm thinking 
    time-of-day for gpm
    time-of-month for g

gpm
    Mostly 0 6pm-6am
    Mostly constant 6am-6pm at say 5 gpm

g
    Increasing from 0 at start of month.

'''


mu = 5
sd = 1
u0 = 1
u1 = 4
pct = .1
N = int(1/pct)


def dhm():
    '''return day-of-month, hour-of-day, minute-of-hour
    '''
    dt = datetime.datetime.fromtimestamp(time.time())
    return (dt.day, dt.hour, dt.minute)


def dm():
    '''return day-of-month, minute-of-day
    '''
    dt = datetime.datetime.fromtimestamp(time.time())
    return (dt.day, 60*dt.hour + dt.minute)


def spiky():
    '''Mostly return 0 with the occasional flush and very occasional much bigger
    flush.
    '''
    if random.choice(range(N)):
        return 0
    if not random.choice(range(N)):
        return random.uniform(u0,u1) * 10
    return random.uniform(u0,u1)


def gpm():
    (d, h, m) = dhm()
    if h < 6 or h > 17:
        return spiky()
    return random.normalvariate(mu,sd)


def gallons_this_month():
    d, m = dm()
    minute_of_month = 60*24*(d-1) + m
    return minute_of_month * random.normalvariate(mu/1.8,sd*0.1)


def signal_strength():
    if not random.choice(range(N)):
        return 0
    if random.choice(range(N)):
        return random.normalvariate(.9,.01)
    return random.uniform(0,1)


def test_internal():
    done = False
    i = 0
    while not done:
        i += 1
        s = spiky()
        print(i, s)
        done = True if s > 10 else False
    print(dhm())
    print(dm())


def test():
    print('gallons this month', gallons_this_month())
    print('               gpm', gpm())
    print('   signal_strength', signal_strength())



