import random
from simu import dhm, dm, minute_of_month, spiky
    

out_example = '''
_|timestamp 1509084250094

f|a_energy_l0_kvah 122.1759
f|a_energy_l1_kvah 122.1759
f|a_energy_l2_kvah 122.1759
f|a_energy_sys_kvah 366.5276

f|a_power_l0_kva 0.1813
f|a_power_l1_kva 0.1813
f|a_power_l2_kva 0.1813
f|a_power_sys_kva 0.544

f|current_avg_sys_a 3.0222

f|current_l0_a 1.0074
f|current_l1_a 1.0074
f|current_l2_a 1.0074

f|energy_l0_kwh 162.9012
f|energy_l1_kwh 162.9012
f|energy_l2_kwh 162.9012
f|energy_sys_kwh 488.7035

f|frequency_hz 59.9793

f|power_l0_kw 0.1209
f|power_l1_kw 0.1209
f|power_l2_kw 0.1209
f|power_sys_kw 0.3627

f|r_energy_l0_kvarh 54.3004
f|r_energy_l1_kvarh 54.3004
f|r_energy_l2_kvarh 54.3004
f|r_energy_sys_kvarh 162.9013

f|r_power_l0_kvar 0.0806
f|r_power_l1_kvar 0.0806
f|r_power_l2_kvar 0.0806
f|r_power_sys_kvar 0.2418

f|voltage_l0ton_v 39.74
f|voltage_l1ton_v 39.79
f|voltage_l2ton_v 40.07
f|voltage_ltol_avg_v 120.93
f|voltage_lton_avg_v 119.51

s|DeviceName PS24-PS240000000-eh
s|deviceId 59c2d771a894ed1461f0e57f
s|telemetryId 59f2cc5ae0c2b6ae52c0b15d
'''


mu = 5
sd = 1
u0 = 1
u1 = 4
pct = .1
N = int(1/pct)


def mostly_flat_during_off_hours():
    '''
    '''
    (d, h, m) = dhm()
    if h < 6 or h > 17:
        return spiky(u0, u1, pct)
    return random.normalvariate(mu,sd)


def increasing_all_month():
    return minute_of_month() * random.normalvariate(mu/1.8,sd*0.1)


def in_01_interval(pct):
    '''
    '''

    if random.uniform(0,1) < pct:
        return 0
    if random.uniform(0,1) > pct:
        return random.normalvariate(.9,.01)
    return random.uniform(0,1)


def test_internal():
    done = False
    i = 0
    while not done:
        i += 1
        s = spiky(u0, u1, pct)
        print(i, s)
        done = True if s > 10 else False
    print(dhm())


def test():
    print('gallons this month', gallons_this_month())
    print('               gpm', gpm())
    print('   signal_strength', signal_strength())


#  https://47lining.atlassian.net/wiki/spaces/~brian.mingus/pages/88997889/Arrow+Connect+Linux+Gateway+...

