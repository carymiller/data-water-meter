from pprint import pprint
from time import sleep
from glob import glob
import json
import sys

import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
ps_3037_configs = glob("/opt/selene/config/devices/powerscout-3037*")

for fname in ps_3037_configs:

    config_dict, payload = {}, {}

    with open(fname) as c:
        config = c.readlines()

        for line in config:
            line = line.replace("\n","")
            key, val = line.split("=")
            config_dict[key] = val

    payload["s|name"] = config_dict["name"]
    payload["s|deviceId"] = config_dict["name"].split("-")[1]
    payload["s|telemetryId"] = str(randint(0, sys.maxint))

    payload_1, payload_2, payload_3 = payload.copy(), payload.copy(), payload.copy()
    
    keys = ["a-energy-l0-kvah", "a-energy-l1-kvah", "a-energy-l2-kvah", "a-energy-sys-kvah", "a-power-l0-kva", "a-power-l1-kva", "a-power-l2-kva", "a-power-sys-kva", "current-avg-sys-a", "current-l0-a"]
    keys_1 = ["current-l1-a", "current-l2-a", "energy-l0-kwh", "energy-l1-kwh", "energy-l2-kwh", "energy-sys-kwh", "frequency-hz" "power-l0-kw", "power-l1-kw"]
    keys_2 = ["power-l2-kw", "power-sys-kw", "r-energy-l0-kvarh", "r-energy-l1-kvarh", "r-energy-l2-kvarh", "r-energy-sys-kvarh", "r-power-l0-kvar", "r-power-l1-kvar", "r-power-l2-kvar", "r-power-sys-kvar"]
    keys_3 = ["voltage-l0ton-v", "voltage-l1ton-v", "voltage-l2ton-v", "voltage-ltol-avg-v", "voltage-lton-avg-v"]

    for key in keys: payload["f|" + key] = str(random())
    for key in keys_1: payload_1["f|" + key] = str(random())
    for key in keys_2: payload_2["f|" + key] = str(random())
    for key in keys_3: payload_3["f|" + key] = str(random())

    payload = str(payload).replace("'",'"')
    payload_1 = str(payload_1).replace("'",'"')
    payload_2 = str(payload_2).replace("'",'"')    
    payload_3 = str(payload_3).replace("'",'"')

    pprint(payload_1)

    
    sock.sendto(payload, ("127.0.0.1", int(config_dict["port"]))); sleep(1)
    sock.sendto(payload_1, ("127.0.0.1", int(config_dict["port"]))); sleep(1)
    sock.sendto(payload_2, ("127.0.0.1", int(config_dict["port"]))); sleep(1)
    sock.sendto(payload_3, ("127.0.0.1", int(config_dict["port"]))); sleep(1)

