'''
The standard practice is to not pass a mutable as a default argument.  Here's
why.
'''
from __future__ import print_function
import time

def f(ts=time.time()):
    '''
    fooeey!  ts is evaluated exactly once.
    ts is not mutable but this illustrates the problem.   Default args are
    evaluated once, at function definition time.
    >>> f()
    1509724978.29
    >>> f()
    1509724978.29
    '''
    print(ts)


def g(dct={}):
    '''
    another cautionary example
    >>> g()
    {}
    >>> g()
    {'x': 'y'}
    '''
    print(dct)
    dct['x'] = 'y'


def h(dct={}):
    try:
        dct['count'] += 1
    except KeyError:
        dct['count'] = 1
    print('function h called %s times' % dct['count'])


def i(dct=dict(count=0)):
    '''
    Mutable defaults can be useful though.   As usual, there's a time for
    everything.
    >>> i()
    function i called 1 times
    >>> i()
    function i called 2 times
    >>> i({'count': 33})
    function i called 34 times
    >>> i()
    function i called 3 times
    '''
    dct['count'] += 1
    print('function i called %s times' % dct['count'])


# something completely different #
import sys
sys.exit = lambda:'yoohoo'  # a monkey patch.


