sudo apt update

sudo apt-get remove --purge oracle-java8-jdk openjdk-7-jre oracle-java7-jdk
sudo rm -rf /usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt/jre
sudo apt-get install openjdk-8-jre

sudo apt upgrade

cd /opt
sudo wget https://content.arrowconnect.io/public/selene/latest/db410c/install.sh
sudo chmod 755 install.sh
sudo ./install.sh


vi /opt/selene/config/devices/self.properties. You need to change the apiKey and
secretKey to match your app instances ApplicationOwnerKey. Make sure to use the
AES-256 encrypted version of the keys you don’t want to expose raw keys in
config file.

apiKey=<Encrypted Api Key>
secretKey=<Encrypted Secret Key>
