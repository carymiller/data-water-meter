sudo yum -y groupinstall 'Development Tools' # otherwise pip won't install.
sudo yum -y install openssl-devel

latest=3.6.4
latest=$1

cd /tmp/
wget https://www.python.org/ftp/python/$latest/Python-$latest.tgz
tar zxvf Python-$latest.tgz
cd Python-$latest
./configure --prefix=/opt/python3
make
make install
cd ..
#sudo rm -rf Python-$latest*

ln -s /opt/python3/bin/python3 /usr/bin/python3
ln -s /opt/python3/bin/pip3 /usr/bin/pip3
# Python 3 is now installed

date > /tmp/installed_python_$latest

